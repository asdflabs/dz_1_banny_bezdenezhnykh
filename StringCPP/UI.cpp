#include "pch.h"

using namespace std;
const short NumberOfMenuItems = 6;

string MenuItems[NumberOfMenuItems]
{
		"����� �����������, � ������� ������ � ��������� ������ ���������",
		"����������� ���������� ���� � ������ ����������� � ����� ���������� ���� � �������� ����������� ����",
		"������� ������� ������ ����� �� ������� ����������� � ���������������� ������� ���������",
		"����� ���� ���������� ����������� � �������� �������",
		"������������� �������� ���� � ������ ��� ������������",
		"����� �� ���������"
};

void DrawMenu()
{
	system("CLS");;
	cout << "��� ������ ������� ������ ���� ������� ��������������� ����� �� ����������: \n\n";
	for (short i = 0; i < NumberOfMenuItems; i++)
	{
		cout << to_string(i + 1) << ". " << MenuItems[i] << endl;
	}
}

int StartMenu(vector<vector<string>> &list)
{
	setlocale(0, "");
	DrawMenu();
	bool exit = false;
	while (!exit)
	{
		short inputKey = _getch();
		system("CLS");
		PrintText(list);
		switch (inputKey)
		{
		case 49: // 1
		{
			PrintNecessarySentences(list);
			_getch();
			DrawMenu();
			break;
		}
		case 50: // 2
		{
			cout << "� ������ ����������� " << GetNumberOfWordsFirstSentence(list) << " ����(a)\n\n";
			PrintWordsFromFirstSentenceWithOddNumberOfLetters(list);
			_getch();
			DrawMenu();
			break;
		}
		case 51: // 3
		{
			PrintFirstNumberInFirstString(list);
			_getch();
			DrawMenu();
			break;
		}
		case 52: // 4
		{
			PrintLastSentenceReversed(list);
			_getch();
			DrawMenu();
			break;
		}
		case 53: // 5
		{
			SwapWordsInFirstThreeSentences(list);
			cout << "����� ���� ������� ���������� \n\n";
			PrintText(list);
			_getch();
			DrawMenu();
			break;
		}

		case 54: //6
			exit = true;

		default:
			break;
		}
	}
	return 0;
}