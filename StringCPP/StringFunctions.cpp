#include "pch.h"

using namespace std;

void TrimString(string &s)
{
	size_t first = s.find_first_not_of(' ');
	if (string::npos == first)
		return;
	size_t last = s.find_last_not_of(' ');
	s = s.substr(first, (last - first + 1));
}

vector<string> SplitString(string input, string delimiter) {
	vector<string> splittedInput;
	int start = 0, end = 0;
	string nextWord;
	while ((end = input.find(delimiter, start)) != -1)
	{
		if ((nextWord = input.substr(start, end - start)) != "")
			splittedInput.push_back(nextWord);
		start = end + delimiter.length();
	}
	nextWord = input.substr(start);
	TrimString(nextWord);
	if (nextWord != "")splittedInput.push_back(nextWord);

	return splittedInput;
}

void SplitTextFromConsole(vector<vector<string>> &list)
{
	string input = "";
	cout << "������� ����� ��� ���������:\n";
	while (true) {
		getline(cin, input);
		if (input.length() == 0) cout << "\n�� ����� ������ �����, ���������� �����:\n";
		else break;
	}
	TrimString(input);
	vector<string> textSentences = SplitString(input, ".");
	for (int i = 0; i < textSentences.size(); i++)
	{
		list.push_back(SplitString(textSentences[i], " "));
	}
}

string ChageBase(int number, int newBase) {
	vector<short> digits;
	string result;
	while (number / newBase != 0) {
		digits.push_back(number % newBase);
		number /= newBase;
	}
	digits.push_back(number);
	for (int i = digits.size() - 1; i >= 0; i--)
	{
		switch (digits[i])
		{
		case 10:
		{
			result += "A";
			break;
		}
		case 11:
		{
			result += "B";
			break;
		}
		case 12:
		{
			result += "C";
			break;
		}
		default:
			result += to_string(digits[i]);
			break;
		}
	}
	return result;
}

void PrintFirstNumberInFirstString(vector<vector<string>> &list)
{
	char *p;
	for (int i = 0; i < list[0].size(); i++)
	{
		strtol(list[0][i].c_str(), &p, 10);
		if (*p == 0) {
			cout << "\n������ ����� ����� � ������ ������ � ���������������� ������� ���������: " << ChageBase(stoi(list[0][i]), 13) << endl;
			return;
		}
	}
	cout << "\n� ������ ������ �� ���� ������� ������ �����";
}

void PrintLastSentenceReversed(vector<vector<string>> &list) {
	int lastSentenceIndex = list.size() - 1;
	cout << "����� ���������� ����������� � �������� �������: \n";
	for (int i = list[lastSentenceIndex].size() - 1; i >= 0; i--)
	{
		cout << " " << list[lastSentenceIndex][i];
	}
	cout << ".\n";
}

void SwapAdjacentWordsInSentence(vector<string> &sentence)
{
	int sentenceSize = sentence.size();
	if (sentenceSize % 2 != 0) sentenceSize--;
	string swap;
	for (int i = 0; i < sentenceSize; i += 2) {
		swap = sentence[i];
		sentence[i] = sentence[i + 1];
		sentence[i + 1] = swap;
	}
}

void PrintText(vector<vector<string>> &list) {
	cout << "������� �����: \n";
	for (int i = 0; i < list.size(); i++)
	{
		for (int j = 0; j < list[i].size(); j++)
		{
			cout << " " << list[i][j];
		}
		cout << "." << endl;
	}
	cout << endl;
}

void SwapWordsInFirstThreeSentences(vector<vector<string>> &list) {
	for (int i = 0; i < 3; i++)
	{
		if (i >= list.size()) { cout << "\n ��������, ����������� ����� " << i+1 << " �� �������\n\n"; break; }
		SwapAdjacentWordsInSentence(list[i]);
	}
}

void PrintNecessarySentences(vector<vector<string>> &list)
{
	char firstChar, lastChar;
	int count = 0;
	cout << "�����������, ������������ � ��������������� ����� � ��� �� ��������: \n\n";
	for (int i = 0; i < list.size(); i++)
	{
		firstChar = list[i][0][0];
		lastChar = list[i][list[i].size() - 1][list[i][list[i].size() - 1].size() - 1];
		if (firstChar == lastChar)
		{
			count++;
			for (int j = 0; j < list[i].size(); j++)
			{
				cout << list[i][j] << " ";
			}
			cout << endl;
		}
	}
	if (count == 0)
	{
		cout << "������, �����������, ��������������� �������, �� �������" << endl;
	}
	else
	{
		cout << endl << "�����������, ��������������� ������� �������: " << count << endl;
	}
}

int GetNumberOfWordsFirstSentence(vector<vector<string>> &list)
{
	int count = 0;
	for (int i = 0; i < list[0].size(); i++)
	{
		count++;
	}
	return count;
}

void PrintWordsFromFirstSentenceWithOddNumberOfLetters(vector<vector<string>> &list)
{
	cout << "����� � �������� ����������� ���� � ������ �����������: \n";
	for (int i = 0; i < list[0].size(); i++)
	{
		if ((list[0][i].size() - 1) % 2 == 0)
		{
			cout << list[0][i] << " ";
		}
	}
}
