#ifndef PCH_H
#define PCH_H

#define _USE_MATH_DEFINES
#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <cmath>
#include <string>
#include <vector>
#include <iomanip>
#include <cstdlib>

int StartMenu(std::vector<std::vector<std::string>> &list);

void SplitTextFromConsole(std::vector<std::vector<std::string>> &list);
void PrintFirstNumberInFirstString(std::vector<std::vector<std::string>> &list);
void PrintLastSentenceReversed(std::vector<std::vector<std::string>> &list);
void SwapWordsInFirstThreeSentences(std::vector<std::vector<std::string>> &list);
void PrintText(std::vector<std::vector<std::string>> &list);
std::string ChageBase(int number, int newBase);

std::vector<std::string> SplitString(std::string input, std::string delimiter);
void TrimString(std::string &s);

void PrintNecessarySentences(std::vector<std::vector<std::string>> &list);
int GetNumberOfWordsFirstSentence(std::vector<std::vector<std::string>> &list);
void PrintWordsFromFirstSentenceWithOddNumberOfLetters(std::vector<std::vector<std::string>> &list);

#endif //PCH_H